#include <stdio.h>
#include <windows.h>
#include <conio.h>

#define HASH_CAPACITY 1000
#define HEAD_NUM 10000
#define abs(a) (a >= 0 ? a : -a)

struct hashSet {
	int * head;
	int * next;
	char ** words;
	int headNum;
	int capacity;
	int size;	
};

	void init(struct hashSet * h);
	void clear(struct hashSet * h); 
	int add(struct hashSet * h, char * x);	
	void resize(struct hashSet * h);
	int contains(struct hashSet * h, char * x);
	int hash(char * x);
	int hashIndex(struct hashSet * h, int hash);
	
int main() {
	char fileName[100], string[256];
	struct hashSet dict;
	FILE *fp;
	
	gets(fileName);
	if ((fp = fopen(fileName, "r")) == NULL) {
		printf("Can't open the file\n");
		return 0;	
	}
	
	init(&dict);
	
	while (!feof(fp)) {
		fscanf(fp, "%s", string);
		add(&dict, string);
	}
	
	fclose(fp);
	
	while (strcmp(string, "exit") != 0) {
		gets(string);
		if (strcmp(string, "exit") != 0) {			
			if (contains(&dict, string)) {
				printf("YES\n");
			} else {
				printf("NO\n");
			}
		}
	}
	
	clear(&dict);
	return 0;
}

void init(struct hashSet * h) {
	h->headNum = HEAD_NUM;
	h->capacity = HASH_CAPACITY;
	h->head = (int*) malloc(h->headNum * sizeof(int));
	h->next = (int*) malloc(h->capacity * sizeof(int));
 	h->words = (char**) malloc(h->capacity * sizeof(char*));
 	h->size = 1;
	}

void clear(struct hashSet * h) {
	free(h->head);
	free(h->next);
	free(h->words);
}
         
int add(struct hashSet * h, char * x) {
 	if (contains(h, x)) {
  	return 0;
	}
	
 	int ind = hashIndex(h, hash(x));
 	h->next[h->size] = h->head[ind];
 	h->words[h->size] = x;
 	h->head[ind] = h->size++;
 	
  if (h->size > h->capacity / 4) {
		resize(h);
	}
	
 	return 1;
}

void resize(struct hashSet * h) {
	int i;
	char ** newWords;
	int * newNext;
	
	newWords = (char**) realloc (h->words, 8 * h->capacity * sizeof(char*));
	newNext = (int*) realloc (h->words, 8 * h->capacity * sizeof(int));
	
 	if (newWords != NULL && newNext != NULL)
 	{
   	h->words = newWords;
   	h->next = newNext;
 	}
 	else
 	{
   	clear(h);
   	printf("������ ������������� ������!");
   	exit (1);
 	}
 	
 	h->capacity *= 8;
}

int contains(struct hashSet * h, char * x) {
	int ind = hashIndex(h, hash(x));
	int i;
	
	if (h->head[ind] < 0 || h->head[ind] > h->capacity) {
		return 0;
	}
	
	for (i = h->head[ind]; i != 0; i = h->next[i]) {
		if (h->words[i] != NULL && strcmp(h->words[i], x) == 0) {
   		return 1;
		}
	}
	return 0;
}

int hash(char * x) {
	unsigned int hash = 0;

  for(; *x; x++)
  	hash = (hash * 1664525) + (unsigned char)(*x) + 1013904223;

  return hash;
}

int hashIndex(struct hashSet * h, int hash) {
	return abs(hash) % h->headNum;
}
